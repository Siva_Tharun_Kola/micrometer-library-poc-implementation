# Micrometer Library POC Implementation

This project serves as a bootable jar for micrometer library (https://micrometer.io/docs/concepts#_purpose).
This can be included in any spring boot project as a gradle dependency and be used to generate micrometer metrics using a predefined annoation.
Ex: Here the @Gauge annotation is used to log gauge metric