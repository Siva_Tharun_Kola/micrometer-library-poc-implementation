import com.example.micrometer.MicrometerApplicationTests
import com.example.micrometer.util.ConversionUtil
import io.prometheus.client.exporter.common.TextFormat
import org.apache.commons.lang3.StringUtils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.http.*
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import org.springframework.web.context.WebApplicationContext
import spock.lang.Specification

import java.util.stream.Collectors

@ActiveProfiles("test")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = MicrometerApplicationTests.class)
class GaugeAspectIntegrationSpec extends Specification {

  private MockMvc mockMvc

  @Value(value = '${local.server.port}')
  int localServerPort

  @Autowired
  TestRestTemplate testRestTemplate

  @Autowired
  private WebApplicationContext webApplicationContext

  HttpHeaders apiHttpHeaders
  HttpHeaders metricsHttpHeaders

  def setup() {
    mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build()
    apiHttpHeaders = new HttpHeaders()
    metricsHttpHeaders = new HttpHeaders()
    apiHttpHeaders.set('Accept', 'application/json')
    metricsHttpHeaders.set('Content-Type', 'text/plain')
  }

  def "test success scenario for gauge metric"() {

    when:
    mockMvc.perform(MockMvcRequestBuilders.post('/v1').contentType(MediaType.APPLICATION_JSON).content(ConversionUtil.convertObjectToJsonString(new Integer('5')))).andReturn().response

    ResponseEntity<String> metricsResponse = testRestTemplate.exchange(getBaseUrl() + "/prometheus?name[]=clr_gauge", HttpMethod.GET, new HttpEntity<>(metricsHttpHeaders), String.class)
    then:
    metricsResponse.statusCode == HttpStatus.OK
    StringUtils.deleteWhitespace(TextFormat.CONTENT_TYPE_004) == metricsResponse.getHeaders().getContentType().toString().toLowerCase()
    List<String> responseLines = Arrays.asList(metricsResponse.getBody().split("\n"))
    String requiredLine = responseLines.stream().filter({
      entry -> entry.contains('clr_gauge{job="1"')
    }).collect(Collectors.toList())[0]
    assert !StringUtils.isEmpty(requiredLine)
    assert requiredLine == 'clr_gauge{job="1",} 1.0'
  }

  def "test failure scenario for gauge metric"() {

    when:
    try {
      mockMvc.perform(MockMvcRequestBuilders.post('/v2').contentType(MediaType.APPLICATION_JSON).content(ConversionUtil.convertObjectToJsonString(new Integer('5')))).andReturn().response
    } catch (Exception ex) {

    }
    ResponseEntity<String> metricsResponse = testRestTemplate.exchange(getBaseUrl() + "/prometheus?name[]=clr_gauge", HttpMethod.GET, new HttpEntity<>(metricsHttpHeaders), String.class)
    then:
    metricsResponse.statusCode == HttpStatus.OK
    StringUtils.deleteWhitespace(TextFormat.CONTENT_TYPE_004) == metricsResponse.getHeaders().getContentType().toString().toLowerCase()
    List<String> responseLines = Arrays.asList(metricsResponse.getBody().split("\n"))
    String requiredLine = responseLines.stream().filter({
      entry -> entry.contains('clr_gauge{job="2"')
    }).collect(Collectors.toList())[0]
    assert !StringUtils.isEmpty(requiredLine)
    assert requiredLine == 'clr_gauge{job="2",} 0.0'
  }

  def getBaseUrl() {
    return "http://localhost:" + localServerPort + "/actuator"
  }


}
