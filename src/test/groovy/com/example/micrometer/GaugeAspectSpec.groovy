package com.example.micrometer

import com.example.micrometer.metrics.aspect.Gauge
import com.example.micrometer.metrics.aspect.GaugeAspect
import io.micrometer.prometheus.PrometheusMeterRegistry
import org.aspectj.lang.JoinPoint
import spock.lang.Specification

class GaugeAspectSpec extends Specification {

  PrometheusMeterRegistry prometheusMeterRegistryMock = Mock()
  GaugeAspect gaugeAspectMock
  Gauge gauge = Mock()
  JoinPoint joinPointMock = Mock()

  def setup() {
    gaugeAspectMock = new GaugeAspect(prometheusMeterRegistryMock)
    gauge.gaugeName() == 'gaugeName'
    gauge.tagName() == 'tagName'
    gauge.tagValue() == 'tagValue'
  }

  def "test afterThrowing with no tagValue present in gaugeCache"() {
    given:
    Throwable throwable = Mock()
    when:
    gaugeAspectMock.afterThrowing(joinPointMock,gauge,throwable)
    then:
    0 * prometheusMeterRegistryMock.gauge(_,_,_)
  }

  def "test afterReturning with no tagValue present in gaugeCache"() {
    when:
    gaugeAspectMock.afterReturning(joinPointMock,gauge)
    then:
    0 * prometheusMeterRegistryMock.gauge(_,_,_)
  }

}
