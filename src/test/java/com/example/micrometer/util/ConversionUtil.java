package com.example.micrometer.util;

import java.io.IOException;

import com.fasterxml.jackson.databind.ObjectMapper;

public class ConversionUtil {

  public static String convertObjectToJsonString(Object object) throws IOException {
    ObjectMapper mapper = new ObjectMapper();
    return mapper.writeValueAsString(object);
  }
}
