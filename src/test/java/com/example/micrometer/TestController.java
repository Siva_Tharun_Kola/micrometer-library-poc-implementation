package com.example.micrometer;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.micrometer.metrics.aspect.Gauge;

@RestController
public class TestController {

  @PostMapping(value = "/v1", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
  @Gauge(gaugeName = "test_gauge", tagName = "job", tagValue = "1")
  public String produceSucessResponse(@RequestBody Object requestObject) {
    return "{\"status\":\"Success\"}";
  }

  @PostMapping(value = "/v2", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
  @Gauge(gaugeName = "test_gauge", tagName = "job", tagValue = "2")
  public String produceExceptionResponse(@RequestBody Object requestObject) throws Exception {
    if (true) {
      throw new Exception("Test Exception");
    }
    return "{\"status\":\"Failure\"}";
  }
}
