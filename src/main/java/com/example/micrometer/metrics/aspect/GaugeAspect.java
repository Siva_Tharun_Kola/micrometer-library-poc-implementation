package com.example.micrometer.metrics.aspect;

import java.util.Arrays;
import java.util.HashMap;
import java.util.concurrent.atomic.AtomicInteger;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.micrometer.metrics.enums.GaugeValue;

import io.micrometer.core.instrument.Tag;
import io.micrometer.prometheus.PrometheusMeterRegistry;

@Aspect
@Component
public class GaugeAspect {

  private final PrometheusMeterRegistry        prometheusMeterRegistry;
  private       HashMap<String, AtomicInteger> gaugeCache = new HashMap<>();

  @Autowired
  public GaugeAspect(PrometheusMeterRegistry prometheusMeterRegistry) {
    this.prometheusMeterRegistry = prometheusMeterRegistry;
  }

  @Before(value = "@annotation(gauge) && execution(* *.*(..))")
  public void before(JoinPoint joinPoint, Gauge gauge) {
    AtomicInteger gaugeValue =
        prometheusMeterRegistry.gauge(gauge.gaugeName(), Arrays.asList(Tag.of(gauge.tagName(), gauge.tagValue())),
            new AtomicInteger(GaugeValue.JOB_INITIAL_STATUS.getStatusCode()));
    gaugeCache.put(gauge.tagValue(), gaugeValue);
  }

  @AfterThrowing(value = "@annotation(gauge)", throwing = "throwable", argNames = "joinPoint,gauge,throwable")
  public void afterThrowing(JoinPoint joinPoint, Gauge gauge, Throwable throwable) {
    if (gaugeCache.containsKey(gauge.tagValue())) {
      AtomicInteger gaugeValue = gaugeCache.get(gauge.tagValue());
      gaugeValue.set(GaugeValue.JOB_INITIAL_STATUS.getStatusCode());
    }
  }

  @AfterReturning(value = "@annotation(gauge)", argNames = "joinPoint,gauge")
  public void afterReturning(JoinPoint joinPoint, Gauge gauge) {
    if (gaugeCache.containsKey(gauge.tagValue())) {
      AtomicInteger gaugeValue = gaugeCache.get(gauge.tagValue());
      gaugeValue.set(GaugeValue.JOB_FINAL_STATUS.getStatusCode());
    }

  }
}
