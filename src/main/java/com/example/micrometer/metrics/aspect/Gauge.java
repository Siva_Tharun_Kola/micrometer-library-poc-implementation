package com.example.micrometer.metrics.aspect;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface Gauge {

  /**
   * This value denotes the name of the
   * gauge name.
   *
   * @return the gauge(Metric) name.
   */
  String gaugeName();

  /**
   * This value denotes the name of the
   * tag for the gauge metric.
   *
   * @return the tag name of the metric.
   */
  String tagName();

  /**
   * This value denotes the value of the
   * tag for the gauge metric.
   *
   * @return the value for the tag name.
   */
  String tagValue();
}
