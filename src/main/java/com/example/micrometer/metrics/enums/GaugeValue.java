package com.example.micrometer.metrics.enums;

public enum GaugeValue {
  JOB_INITIAL_STATUS(0),
  JOB_FINAL_STATUS(1);

  private final int statusCode;

  private GaugeValue(int statusCode) {
    this.statusCode = statusCode;
  }

  public int getStatusCode() {
    return statusCode;
  }

  public static GaugeValue getStatus(int statusCode) {
    switch (statusCode) {
      case 0:
        return GaugeValue.JOB_INITIAL_STATUS;
      case 1:
        return GaugeValue.JOB_FINAL_STATUS;
      default:
        return null;
    }
  }

}
